#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import time
import logging
import logging.config
import re

import cmdline
import config
import files
import db
import utils

def mergeFileListAndDbEntries(mergedDbEntries, originalDbEntries, fileList):
    startTime = time.time()
    
    logger = logging.getLogger(__name__) 
    logger.info(u"Merging file/dir list and DB ... ")

    # make a copy of original argument in order to avoid external value modification
    originalDbEntries = originalDbEntries.copy()
    
    numOriginalDbEntries = len(originalDbEntries)
    numPreservedDBEntries = 0
    numAddedDBEntries = 0
    numObsoleteDBEntries = 0
    numRemovedDBEntries = 0
    
    for file in fileList:
        dbKey = db.Key(file.d_type, file.d_fileName)
        
        if dbKey in originalDbEntries:
            # file is already in DB - refreshed DB entry 
            mergedDbEntries[dbKey] = originalDbEntries[dbKey]
            mergedDbEntries[dbKey].d_fileCreationTimeSec = file.d_creationTimeSec
            del originalDbEntries[dbKey]
            numPreservedDBEntries += 1
        else:
            # file is not in DB - new DB entry
            mergedDbEntries[dbKey] = db.Entry(file.d_type,
                                              file.d_fileName,
                                              file.d_creationTimeSec)
            numAddedDBEntries += 1
    
    # handle DB entries those have no corresponding files 
    for dbKey in originalDbEntries:
        entry = originalDbEntries[dbKey]
        
        if entry.isModifiedByUser():
            # DB entry labeled as obsolete because it was modified by used
            entry.d_obsolete = True
            mergedDbEntries[dbKey] = entry
            numObsoleteDBEntries += 1
        else:
            # DB entry wiped from DB because it's NOT modified by used
            numRemovedDBEntries += 1

    logger.info(u"Merging file/dir list and DB is complete in {0:.2f} sec. Entries in ...\n"
                u"  original DB .......................................... {1:>6d}\n"
                u"  original file/dir list ............................... {2:>6d}\n"
                u"  merged DB (preserved/added/obsolete/removed/total) ... {3:>6d}/ {4:>6d} /{5:>6d} /{6:>6d} /{7:>6d}\n".
                format(time.time() - startTime,
                       numOriginalDbEntries,
                       len(fileList),
                       numPreservedDBEntries, numAddedDBEntries, numObsoleteDBEntries, numRemovedDBEntries, len(mergedDbEntries)))

    
def main():
    startTime = time.time()

    utils.setupConsoleEncodig()
    utils.setupYamlLoader()

    args = cmdline.parseCommandLine()
    configFileName = os.path.abspath(args.d_configFileName)
    print("Config file: '{0}'\n".format(configFileName))
    
    cfg = {}
    error = config.loadFromFile(cfg, configFileName)
    if error is not None:
        print("Config file loading failed. " + error)
        return

    error = config.verify(cfg)
    if error is not None:
        print("Config verification failed. " + error)
        return

    config.normalize(cfg)

    # init logging
    if config.kLogging in cfg:
        logging.config.dictConfig(cfg[config.kLogging])

    logger = logging.getLogger(__name__)

    fileList = []
    for eSrc in cfg[config.kFiles]:

        includeFiles = []
        for e in eSrc[config.kIncludeFiles]:
            includeFiles.append(re.compile(e))

        includeDirs = []
        for e in eSrc[config.kIncludeDirs]:
            includeDirs.append(re.compile(e))

        excludeFiles = []
        for e in eSrc[config.kExcludeFiles]:
            excludeFiles.append(re.compile(e))

        excludeDirs = []
        for e in eSrc[config.kExcludeDirs]:
            excludeDirs.append(re.compile(e))

        fl = []
        files.readFileList(
            fl,
            eSrc[config.kSrcFileDir],
            includeFiles, includeDirs, excludeFiles, excludeDirs)
        
        fileList.extend(fl)

    originalDbEntries = {}
    db.read(cfg[config.kDatabase][config.kDbFileName], originalDbEntries)

    mergedDbEntries = {}
    mergeFileListAndDbEntries(mergedDbEntries, originalDbEntries, fileList)

    db.transformLinks2Tags(mergedDbEntries, cfg[config.kTags])

    db.write(cfg[config.kDatabase][config.kDbFileName],
             mergedDbEntries,
             cfg[config.kDatabase][config.kDbBackupCount])
    
    logger.info(u"Finished in {0:.2f} sec.\n".
                format(time.time() - startTime))

if __name__ == "__main__":
    main();
