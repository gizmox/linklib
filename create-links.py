#!/usr/bin/python3

import time
import os
import logging
import logging.config

import cmdline
import config
import db
import utils
from entrytype import EntryType

def cleanDir(dir):
    startTime = time.time()

    absDir = os.path.abspath(dir)

    logger = logging.getLogger(__name__)
    logger.info(u"Links cleaning in '{0}' ...".format(absDir))

    numLinksToFiles = 0
    numLinksToDirs = 0
    numFiles = 0
    numDirs = 0

    for root, dirs, files in os.walk(absDir, topdown=False):
        for f in files:
            path = os.path.join(root, f)
            if os.path.islink(path):
                os.unlink(path)
                numLinksToFiles += 1
            else:
                os.remove(path)
                numFiles += 1

        for d in dirs:
            path = os.path.join(root, d)
            if os.path.islink(path):
                os.unlink(path)
                numLinksToDirs += 1
            else:
                os.rmdir(path)
                numDirs += 1

    logger.info(u"Links cleaning is complete in {0:.2f} sec\n"
                u"  directory ............................ '{1}'\n"
                u"  removed files/dirs/total .............. {2:>6d} /{3:>6d} /{4:>6d}\n"
                u"  removed links to (files/dirs/total) ... {5:>6d} /{6:>6d} /{7:>6d}\n"
                u"  removed entries total ................. {8:>6d}\n".
                format(time.time() - startTime,
                       absDir,
                       numFiles, numDirs, numFiles + numDirs,
                       numLinksToFiles, numLinksToDirs, numLinksToFiles + numLinksToDirs,
                       numFiles + numDirs + numLinksToFiles + numLinksToDirs))

def ensureDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def createLinks(dbEntries, linkRootDir, tagDict):
    startTime = time.time()

    linkRootAbsDir = os.path.abspath(linkRootDir)

    logger = logging.getLogger(__name__)
    logger.info(u"Links creation in '{0}'".format(linkRootAbsDir))

    numToBeCreated = 0
    numObsolete = 0
    numFileLinksCreated = 0
    numDirLinksCreated = 0
    numLinksNotCreated = 0
    
    for k in dbEntries:
        entry = dbEntries[k]
        if entry.d_obsolete:
            numObsolete += 1
            continue
        
        s, fileExt = os.path.splitext(entry.d_fileName)

        for ln in entry.d_linkNames:
            if not ln:
                continue

            numToBeCreated += 1

            linkAbsName = os.path.join(linkRootAbsDir, ln)
            
            s, linkExt = os.path.splitext(linkAbsName)
            if entry.d_fileType == EntryType.file and fileExt != linkExt:
                linkAbsName += fileExt
                
            ensureDir(os.path.dirname(linkAbsName))
            os.symlink(entry.d_fileName, linkAbsName)

            # check created link
            if os.path.isfile(linkAbsName):
                numFileLinksCreated += 1
            elif os.path.isdir(linkAbsName):
                numDirLinksCreated += 1
            else:
                logger.error(u"failed to create link '{0}'->'{1}'".format(linkAbsName, entry.d_fileName))
                numLinksNotCreated += 1

        if entry.d_tagLink and entry.d_tags:
            linkName = entry.d_tagLink
            s, linkExt = os.path.splitext(linkName)
            if entry.d_fileType == EntryType.file and fileExt != linkExt:
                linkName += fileExt

            for t in entry.d_tags:
                numToBeCreated += 1

                if t in tagDict:
                    linkAbsName = os.path.join(linkRootAbsDir, tagDict[t], linkName)
                    ensureDir(os.path.dirname(linkAbsName))
                    os.symlink(entry.d_fileName, linkAbsName)

                    # check created link
                    if os.path.isfile(linkAbsName):
                        numFileLinksCreated += 1
                    elif os.path.isdir(linkAbsName):
                        numDirLinksCreated += 1
                    else:
                        logger.error(u"failed to create link '{0}'->'{1}'".format(linkAbsName, entry.d_fileName))
                        numLinksNotCreated += 1
                else:
                    logger.error(u"tag '{0}' is not configured".format(t))
                    numLinksNotCreated += 1

    logger.info(u"Links creation is complete in {0:.2f} sec.\n"
                u"  destination dir ..................'{1}'\n"
                u"  entries in DB (obsolete/total) ... {2:>6d} /{3:>6d}\n"
                u"Links ...\n"
                u"  were supposed to be created ...... {4:>6d}\n"
                u"  creation failures ................ {5:>6d}\n"
                u"  created (files/dirs/total) ....... {6:>6d} /{7:>6d} /{8:>6d}\n".
                format(time.time() - startTime,
                       linkRootAbsDir,
                       numObsolete, len(dbEntries),
                       numToBeCreated,
                       numLinksNotCreated,
                       numFileLinksCreated, numDirLinksCreated, numFileLinksCreated + numDirLinksCreated))

         
def main():
    startTime = time.time()

    utils.setupConsoleEncodig()
    utils.setupYamlLoader()

    args = cmdline.parseCommandLine()
    configFileName = os.path.abspath(args.d_configFileName)
    print("Config file: '{0}'\n".format(configFileName))
    
    cfg = {}
    error = config.loadFromFile(cfg, configFileName)
    if error is not None:
        print("Config file loading failed. " + error)
        return
    
    error = config.verify(cfg)
    if error is not None:
        print("Config verification failed. " + error)
        return
    
    config.normalize(cfg)
    
    # init logging
    if config.kLogging in cfg:
        logging.config.dictConfig(cfg[config.kLogging])

    logger = logging.getLogger(__name__)

    dbEntries = {}
    db.read(cfg[config.kDatabase][config.kDbFileName], dbEntries)
    
    cleanDir(cfg[config.kLinks][config.kDstLinksDir])
    createLinks(dbEntries,
                cfg[config.kLinks][config.kDstLinksDir],
                cfg[config.kTags] if config.kTags in cfg else {})

    logger.info(u"Finished in {0:.2f} sec.\n".
                format(time.time() - startTime))

if __name__ == "__main__":
    main()
