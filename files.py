# -*- coding: utf-8 -*-

import os
import logging
import time

from entrytype import *

class Entry:
    __slots__ = ["d_fileName", "d_type", "d_creationTimeSec"]

    def __init__(self, fileName, type, creationTimeSec):
        self.d_fileName = fileName
        self.d_type = type
        self.d_creationTimeSec = creationTimeSec
    
    def __repr__(self):
        return unicode(self).encode('utf-8')

    def __str__(self):
        return unicode(self).encode('utf-8')
    
    def __unicode__(self):
        return u"<{0}:{1}:{2}>".format(self.d_type,
                                       self.d_creationTimeSec,
                                       self.d_fileName)

def checkFilters(s, includeFilter, excludeFilter):
    for e in includeFilter:
        if not e.match(s) is None:
            # found in one of include-filters

            for e in excludeFilter:
                if not e.match(s) is None:
                    # found in one of exclude-filters
                    return False

            # not found in exclude-filters
            return True

    # not found in include-filters
    return False

def readFileList(fileList, fileRootDir, includeFiles, includeDirs, excludeFiles, excludeDirs):
    startTime = time.time()
    
    logger = logging.getLogger(__name__)
    
    if not os.path.isdir(fileRootDir):
        logger.warn(u"Source directory not found: '{0}'".format(fileRootDir))
        return
    
    logger.info(u"File/dir list reading from '{0}' ...".format(fileRootDir))

    numDirsFound = 0
    numFilesFound = 0
    for currentDir, subDirs, files in os.walk(top=fileRootDir,
                                              followlinks=False,
                                              topdown=False):
        numDirsFound += len(subDirs)
        numFilesFound += len(files)

    numDirLinksSkipped = 0
    numFileLinksSkipped = 0
    numDirsFiltered = 0
    numFilesFiltered = 0
    numDirsInResult = 0
    numFilesInResult = 0
    
    fl = []
    for currentDir, subDirs, files in os.walk(top=fileRootDir, 
                                              followlinks=False, 
                                              topdown=True):
        filteredSubDirs = []
        for sd in subDirs:
            absPath = os.path.join(currentDir, sd)
            s = absPath[len(fileRootDir) + 1:]
            if not checkFilters(s, includeDirs, excludeDirs):
                continue

            filteredSubDirs.append(sd)

        numDirsFiltered += (len(subDirs) - len(filteredSubDirs))
        subDirs[:] = filteredSubDirs

        if os.path.islink(currentDir):
            numDirLinksSkipped += 1
            continue

        fl.append(Entry(currentDir, EntryType.dir, os.path.getctime(currentDir)))
        numDirsInResult += 1
        
        for file in files:
            absFileName = os.path.join(currentDir, file)

            logger.info(absFileName)

            if os.path.islink(absFileName):
                numFileLinksSkipped += 1
                print("skipped as link")
                continue

            s = absFileName[len(fileRootDir) + 1:]
            if not checkFilters(s, includeFiles, excludeFiles):
                numFilesFiltered += 1
                print("skipped as filter")
                continue

            print("accepted")

            fl.append(Entry(absFileName, EntryType.file, os.path.getctime(absFileName)))
            numFilesInResult += 1

    fileList.extend(fl)
 
    logger.info(u"File/dir list reading is complete in {0:.2f} sec:\n"
                u"  source  ................................... '{1}'\n"
                u"  total in source dir (files / dirs) ......... {2:>6d} /{3:>6d}\n"
                u"  filtered by configuration (files / dirs) ... {4:>6d} /{5:>6d}\n"
                u"  links ignored (files / dirs / total) ....... {6:>6d} /{7:>6d} /{8:>6d}\n"
                u"  entries read (files / dirs / total) ........ {9:>6d} /{10:>6d} /{11:>6d}\n".
                format(time.time() - startTime,
                       fileRootDir,
                       numFilesFound, numDirsFound,
                       numFilesFiltered, numDirsFiltered,
                       numFileLinksSkipped, numDirLinksSkipped, numFileLinksSkipped + numDirLinksSkipped,
                       numFilesInResult, numDirsInResult, len(fl)))
