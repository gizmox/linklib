# -*- coding: utf-8 -*-

import os
import time
import datetime
import logging
import glob
import shutil
import filecmp
from entrytype import *

import yaml

def convertFloatSec2DateTime(sec):
    return datetime.datetime.strptime(time.ctime(sec), u"%a %b %d %H:%M:%S %Y")

def convertDateTime2FloatSec(dt):
    return time.mktime(dt.timetuple()) # + dt.microsecond / 1E6

def formatDateTime4DbEntry(dt):
    return dt.strftime(u"%Y-%m-%d %H:%M:%S")

def formatDateTime4BackupSuffix(dt):
    return dt.strftime(u"%Y%m%d%H%M%S%f")

def parseFileCreationDateTime(s):
    return datetime.datetime.strptime(s, u"%Y-%m-%d %H:%M:%S")

class Key:
    __slots__ = ["d_fileType", "d_fileName"];

    def __init__(self, fileType, fileName):
        self.d_fileType = fileType
        self.d_fileName = fileName
        
    def __eq__(self, another):
        return self.d_fileType == another.d_fileType and \
               self.d_fileName == another.d_fileName
               
    def __hash__(self):
        return hash(self.d_fileType) + hash(self.d_fileName)
    
    def __repr__(self):
        return unicode(self).encode('utf-8')

    def __str__(self):
        return unicode(self).encode('utf-8')
    
    def __unicode__(self):
        return u"<{0}:{1}>".format(self.d_fileType, self.d_fileName)


class Entry:
    __slots__ = ["d_fileType", "d_fileName", "d_fileCreationTimeSec", "d_linkNames", "d_tagLink", "d_tags", "d_obsolete"]

    def __init__(self,
                 fileType,
                 fileName,
                 fileCreationTimeSec,
                 linkNames = [],
                 tagLink = u"",
                 tags = [],
                 obsolete = False):
        self.d_fileType = fileType
        self.d_fileName = fileName
        self.d_fileCreationTimeSec = fileCreationTimeSec
        self.d_linkNames = linkNames
        self.d_tagLink = tagLink
        self.d_tags = tags
        self.d_obsolete = obsolete
        
    def isModifiedByUser(self):
        for e in self.d_linkNames:
            if e:
                return True

        if self.d_tagLink:
            return True

        for e in self.d_tags:
            if e:
                return True

        return False
        
    def __repr__(self):
        return unicode(self).encode('utf-8')

    def __str__(self):
        return unicode(self).encode('utf-8')
    
    def __unicode__(self):
        return u"<[{0}]:{1}:{2}>".format("-" if self.d_obsolete else "+",
                                         self.d_fileType,
                                         self.d_fileName)

def prepareDbBackup(dbFileName, backupCount):
    dbAbsFileName = os.path.abspath(dbFileName)
    backupFiles = glob.glob(dbAbsFileName + ".bak.*")

    if backupCount is 0:
        # remove all backup copies and do nothing
        for f in backupFiles:
            os.remove(f)
        return

    # first: do backup

    # newer files come first
    backupFiles.sort(reverse=True)

    if backupFiles:
        # compare original file and latest backup
        doBackup = not filecmp.cmp(dbAbsFileName, os.path.abspath(backupFiles[0]), shallow=False)
    else:
        doBackup = True

    if doBackup:
        dbModificationTimeSec = os.path.getctime(dbAbsFileName)
        backupSuffix = formatDateTime4BackupSuffix(convertFloatSec2DateTime(dbModificationTimeSec))
        shutil.copy(dbAbsFileName, dbAbsFileName + ".bak." + backupSuffix)

    # then: remove old backup files
    while len(backupFiles) >= backupCount:
        os.remove(backupFiles[0])
        backupFiles.pop(0)

def read(dbFileName, dbEntries):
    startTime = time.time()
    logger = logging.getLogger(__name__)

    dbAbsFileName = os.path.abspath(dbFileName)

    if not os.path.isfile(dbAbsFileName):
        logger.warn(u"DB file not found: '{0}'".format(dbFileName))
        return
    
    logger.info(u"DB reading from '{0}' ...".format(dbAbsFileName))
    
    db = []
    with open(dbAbsFileName, 'r') as yaml_file:
        db = yaml.load(yaml_file, Loader=yaml.FullLoader)

    numFiles = 0
    numDirs = 0
    numTotalEntries = 0
    numObsoleteFiles = 0
    numObsoleteDirs = 0
        
    for e in db:
        entryType = EntryType.file if e["type"] == "file" else EntryType.dir
        numFiles += 1 if entryType == EntryType.file else 0
        numDirs += 1 if entryType == EntryType.dir else 0
        
        obsolete = True if "obsolete" in e and e["obsolete"] == "true" else False    

        if obsolete:
            if entryType == EntryType.file:
                numObsoleteFiles += 1
            else:
                numObsoleteDirs += 1
        
        fileCreationTimeSec = convertDateTime2FloatSec(
                                  parseFileCreationDateTime(
                                      e["fileCreationTime"]))

        key = Key(entryType, e["file"])
        entry = Entry(entryType,
                      e["file"],
                      fileCreationTimeSec,
                      e["links"] if "links" in e else [],
                      e["tagLink"] if "tagLink" in e else "",
                      e["tags"] if "tags" in e else [],
                      obsolete)

        if key in dbEntries:
            if entry.isModifiedByUser() and (not dbEntries[key].isModifiedByUser()):
                dbEntries[key] = entry
            elif (not entry.isModifiedByUser()) and dbEntries[key].isModifiedByUser():
                None
            elif (not entry.isModifiedByUser()) and (not dbEntries[key].isModifiedByUser()):
                None
            elif entry.isModifiedByUser() and dbEntries[key].isModifiedByUser():
                assert False
        else:
            dbEntries[key] = entry

        numTotalEntries += 1

    logger.info(u"DB reading is complete in {0:.2f} sec\n"
                u"  source .................................... '{1}'\n"
                u"  obsolete records (files / dirs / total) ... {2:>6d} /{3:>6d} /{4:>6d}\n"
                u"  read records (files / dirs / total) ....... {5:>6d} /{6:>6d} /{7:>6d}\n".
                format(time.time() - startTime,
                       dbAbsFileName,
                       numObsoleteFiles, numObsoleteDirs, numObsoleteFiles + numObsoleteDirs,
                       numFiles, numDirs, numTotalEntries))

def transformLinks2Tags(dbEntries, tagDict):
    logger = logging.getLogger(__name__)

    for key in dbEntries:
        entry = dbEntries[key]

        tagLink = u""
        tags = []

        for i, ln in enumerate(entry.d_linkNames):
            if not ln:
                continue

            (path, link) = os.path.split(ln)

            if not tagLink:
                tagLink = link
            else:
                if tagLink != link:
                    logger.error(u"Different link names '{0}'".format(link))
                    continue

            path = os.path.normpath(path)
            tagWasFound = False
            for t in tagDict:
                if path == tagDict[t]:
                    tags.append(t)
                    tagWasFound = True

            if tagWasFound:
                entry.d_linkNames[i] = u""
                entry.d_tagLink = tagLink
                entry.d_tags = tags
            else:
                logger.error(u"Untagged folder '{0}'".format(path))

        entry.d_linkNames = [x for x in entry.d_linkNames if x != u""]

def write(dbFileName, dbEntries, backupCount):
    startTime = time.time()
    logger = logging.getLogger(__name__)

    dbAbsFileName = os.path.abspath(dbFileName)

    logger.info(u"DB writing to '{0}' ...".format(dbAbsFileName))
    
    result = []
    for k in dbEntries:
        v = dbEntries[k]

        m = {}
                
        if v.d_obsolete: 
            m['obsolete'] = u"true"
              
        m['type'] = "file" if v.d_fileType == EntryType.file else "dir"
        m['file'] = v.d_fileName
        m['fileCreationTime'] = formatDateTime4DbEntry(
                                    convertFloatSec2DateTime(
                                        v.d_fileCreationTimeSec))
        if len(v.d_linkNames) > 0:
            m['links'] = v.d_linkNames

        m['tagLink'] = v.d_tagLink
        m['tags'] = v.d_tags
        
        result.append(m)

    result.sort(key = lambda e: e['fileCreationTime'], reverse=True)

    prepareDbBackup(dbAbsFileName, backupCount)

    with open(dbAbsFileName, 'w') as yaml_file:
#        yaml_file.write(yaml.dump(result, encoding='utf-8', default_flow_style=False))
#         yaml_file.write(yaml.safe_dump(result, 
#                                        default_style='"', 
#                                        allow_unicode=True, 
#                                        default_flow_style=False,
#                                        width=999999))
        dumper = yaml.SafeDumper
        dumper.ignore_aliases = lambda self, data: True
        yaml_file.write(yaml.dump(result, 
                                  default_style='"', 
                                  allow_unicode=True, 
                                  default_flow_style=False,
                                  width=999999,
                                  Dumper=dumper))
    
    logger.info(u"DB writing is complete in {0:.2f} sec\n"
                u"  destination ....... '{1}'\n"
                u"  entries written ... {2:>6d}\n".
                format(time.time() - startTime,
                       dbAbsFileName,
                       len(result)))

def removeObsolete(dstDbEntries, srcDbEntries):
    startTime = time.time()

    logger = logging.getLogger(__name__)
    logger.info(u"Removing obsolete records ... ")

    numRemovedObsoleteEntries = 0

    yesAll = False
    noAll = False

    entries = {}

    interrupted = False
    for key in srcDbEntries:
        entry = srcDbEntries[key]

        if not entry.d_obsolete:
            entries[key] = entry
            continue

        answer = ""
        if (not yesAll) and (not noAll):
            while True:
                answer = raw_input(u"'{0}'->'{1}'\n"
                                   u"Delete? Press (y)yes / (Y)yes all / (n)no / (N)no all / (s)stop: ".
                                   format(entry.d_tagLink, entry.d_fileName))
                if answer in ["y", "Y", "n", "N", "s"]:
                    break
                else:
                    print("Unrecognized input.")

            print

            if answer == "Y":
                yesAll = True
            elif answer == "N":
                if numRemovedObsoleteEntries == 0:
                    break

                noAll = True
            elif answer == "s":
                interrupted = True
                break

        if yesAll or (answer == "y"):
            # skip the entry and don't copy
            numRemovedObsoleteEntries += 1
        elif noAll or (answer == "n"):
            # copy entry
            entries[key] = entry

    if interrupted:
        numRemovedObsoleteEntries = 0

    if numRemovedObsoleteEntries > 0:
        answer = ""
        while True:
            answer = raw_input(u"{0} obsolete entries is about to be deleted.\n"
                               u"Commit changes? Press (y)yes / (n)no: ".
                               format(numRemovedObsoleteEntries))
            if answer in ["y", "n"]:
                break
            else:
                print("Unrecognized input.")

        print()

        if answer == "y":
            dstDbEntries.clear()
            dstDbEntries.update(entries)
        else:
            numRemovedObsoleteEntries = 0

    logger.info(u"Removing obsolete records is complete in {0:.2f} sec.\n"
                u"  obsolete entries removed ... {1:>6d}\n"
                u"  entries before/after ....... {2:>6d}/ {3:>6d}\n"
                u"{4}\n".
                format(time.time() - startTime,
                       numRemovedObsoleteEntries,
                       len(srcDbEntries), len(srcDbEntries) - numRemovedObsoleteEntries,
                       u"No DB update is required." if numRemovedObsoleteEntries == 0 else u"DB update is required."))

    return (numRemovedObsoleteEntries > 0)


class BaseFileNameKey:
    __slots__ = ["d_fileType", "d_baseFileName"];

    def __init__(self, fileType, baseFileName):
        self.d_fileType = fileType
        self.d_baseFileName = baseFileName

    def __eq__(self, another):
        return self.d_fileType     == another.d_fileType and \
               self.d_baseFileName == another.d_baseFileName

    def __hash__(self):
        return hash(self.d_fileType) + hash(self.d_baseFileName)

    def __repr__(self):
        return unicode(self).encode('utf-8')

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return u"<{0}:{1}>".format(self.d_fileType, self.d_baseFileName)

def getBaseFileNameFromPath(path):
    return os.path.basename(path)

def getDirFromPath(path):
    return os.path.dirname(path)

def mergeObsoleteToUnmodified(dstDbEntries, srcDbEntries):
    startTime = time.time()

    logger = logging.getLogger(__name__)
    logger.info(u"Merging obsolete to unmodified records ...")

    numObsoleteIgnoredEntries = 0
    numUnmodifiedIgnoredEntries = 0
    numMergedEntries = 0

    yesAll = False
    noAll = False

    resultDbEntries = {}

    obsoleteDbEntries = {}
    unmodifiedDbEntries = {}

    for dbKey in srcDbEntries:
        dbEntry = srcDbEntries[dbKey]

        baseFileNameKey = BaseFileNameKey(dbKey.d_fileType,
                                          getBaseFileNameFromPath(dbKey.d_fileName))

        if dbEntry.d_obsolete and dbEntry.isModifiedByUser():
            baseFileNameKey = BaseFileNameKey(dbKey.d_fileType,
                                              getBaseFileNameFromPath(dbKey.d_fileName))

            if not (baseFileNameKey in obsoleteDbEntries):
                obsoleteDbEntries[baseFileNameKey] = []

            obsoleteDbEntries[baseFileNameKey].append(dbEntry)

        elif (not dbEntry.d_obsolete) and (not dbEntry.isModifiedByUser()):
            baseFileNameKey = BaseFileNameKey(dbKey.d_fileType,
                                              getBaseFileNameFromPath(dbKey.d_fileName))

            if not (baseFileNameKey in unmodifiedDbEntries):
                unmodifiedDbEntries[baseFileNameKey] = []

            unmodifiedDbEntries[baseFileNameKey].append(dbEntry)

        else:
            resultDbEntries[dbKey] = dbEntry

    interrupted = False
    i = 0
    for baseFileNameKey in obsoleteDbEntries:
        i += 1
        obsoleteList = obsoleteDbEntries[baseFileNameKey]

        unmodifiedList = []
        if baseFileNameKey in unmodifiedDbEntries:
            unmodifiedList = unmodifiedDbEntries[baseFileNameKey]

        if len(obsoleteList) > 1 or len(unmodifiedList) > 1:
            logger.info(u"Ignoring duplicate entries: \n"
                        u"  '{0}'\n"
                        u"  ignored (obsolete/unmodified) {1:>6d}/ {2:>6d}\n".
                        format(baseFileNameKey,
                               len(obsoleteList),
                               len(unmodifiedList)))

            numObsoleteIgnoredEntries += len(obsoleteList)
            numUnmodifiedIgnoredEntries += len(unmodifiedList)

            # put duplicated entries back to db unmodified
            for obsolete in obsoleteList:
                resultDbEntries[Key(obsolete.d_fileType, obsolete.d_fileName)] = obsolete
            for unmodified in unmodifiedList:
                resultDbEntries[Key(unmodified.d_fileType, unmodified.d_fileName)] = unmodified

            continue

        obsolete = obsoleteList[0]

        if len(unmodifiedList) == 0:
            # obsolete entry has no corresponding unmodified entry
            resultDbEntries[Key(obsolete.d_fileType, obsolete.d_fileName)] = obsolete
            continue

        unmodified = unmodifiedList[0]

        answer = ""
        if (not yesAll) and (not noAll):
            while True:
                answer = raw_input(u"File: {0}\n"
                                   u"Obsolete entry ({1:>6d}/ {2:>6d})\n"
                                   u"  path: '{3}'\n"
                                   u"  link: '{4}'\n"
                                   u"  tags: {5}\n" 
                                   u"Unmodified entry\n"
                                   u"  path: '{6}'\n"
                                   u"Merge? Press (y)yes / (Y)yes all / (n)no / (N)no all / (s)stop: ".
                                   format(baseFileNameKey,                        # {0} base file name
                                          i, len(obsoleteDbEntries),              # {1}, {2}
                                          getDirFromPath(obsolete.d_fileName),    # {3} obsolete entry file path
                                          obsolete.d_tagLink,                     # {4} obsolete entry link
                                          obsolete.d_tags,                        # {5} obsolete entry tags
                                          getDirFromPath(unmodified.d_fileName))) # {6} unmodified entry file path
                if answer in ["y", "Y", "n", "N", "s"]:
                    break
                else:
                    print("Unrecognized input.")

            print()

            if answer == "Y":
                yesAll = True
            elif answer == "N":
                noAll = True
            elif answer == "s":
                interrupted = True
                break

        if yesAll or (answer == "y"):
            # merge obsolete entry to unmodified
            unmodified.d_tagLink = obsolete.d_tagLink
            unmodified.d_tags = obsolete.d_tags
            resultDbEntries[Key(unmodified.d_fileType, unmodified.d_fileName)] = unmodified

            del unmodifiedDbEntries[baseFileNameKey]

            numMergedEntries += 1

        elif noAll or (answer == "n"):
            # merge obsolete entry to unmodified
            resultDbEntries[Key(obsolete.d_fileType, obsolete.d_fileName)] = obsolete
            resultDbEntries[Key(unmodified.d_fileType, unmodified.d_fileName)] = unmodified

    if (not interrupted) and numMergedEntries > 0:
        answer = ""
        while True:
            answer = raw_input(u"{0} merged entries is about to be written to DB.\n"
                               u"Commit changes? Press (y)yes / (n)no: ".
                               format(numMergedEntries))
            if answer in ["y", "n"]:
                break
            else:
                print("Unrecognized input.")

        print()

        if answer == "y":
            dstDbEntries.clear()
            dstDbEntries.update(resultDbEntries)
        else:
            numMergedEntries = 0

    logger.info(u"Merging obsolete to unmodified records is {0} in {1:.2f} sec.\n"
                u"  merged pairs (obsolete-unmodified) .. {2:>6d}\n"
                u"  entries before/after ................ {3:>6d}/ {4:>6d}\n"
                u"{5}\n".
                format(u"stopped" if interrupted else u"complete",
                       time.time() - startTime,
                       numMergedEntries,
                       len(srcDbEntries), len(dstDbEntries),
                       u"No DB update is required." if (interrupted or numMergedEntries == 0) else u"DB update is required."))

    return (numMergedEntries > 0)
