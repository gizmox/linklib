# -*- coding: utf-8 -*-

import os.path
import yaml
import codecs

kFiles         = "files"
kSrcFileDir    = "srcFileDir"
kDstLinksDir   = "dstLinksDir"
kIncludeFiles  = "includeFiles"
kExcludeFiles  = "excludeFiles"
kIncludeDirs   = "includeDirs"
kExcludeDirs   = "excludeDirs"

kLinks         = "links"
kTags          = "tags"

kDatabase      = "database"
kDbFileName    = "dbFileName"
kDbBackupCount    = "dbBackupCount"

kLogging       = "logging"

def loadFromFile(config, configFileName):
    if not os.path.isfile(configFileName):
        return "File not found: '{0}'".format(configFileName)

#    with codecs.open(configFileName, 'r', "utf-8") as yaml_file:
    with open(configFileName, 'r') as yaml_file:
        cfg = yaml.load(yaml_file, Loader=yaml.FullLoader)

    config.update(cfg)
    
    return None

def verify(config):
    
    # -------------------------------------------------------------------------
    if kFiles not in config:
        return "Entry '{0}' is not found".format(kFiles)
    
    eFiles = config[kFiles]
    
    if not eFiles:
        return "Entry '{0}' is empty".format(kFiles)
    
    for iSrc, eSrc in enumerate(eFiles):
        if kSrcFileDir not in eSrc:
            return "Entry '{0}/#{1}/{2}' is not found".format(kFiles, iSrc, kSrcFileDir)
        
        if not eSrc[kSrcFileDir]:
            return "Entry '{0}/#{1}/{2}' is empty".format(kFiles, iSrc, kSrcFileDir)
    
    # -------------------------------------------------------------------------
    if kLinks not in config:
        return "Entry '{0}' is not found".format(kLinks)
    
    eLinks = config[kLinks]
    
    if kDstLinksDir not in eLinks:
        return "Entry '{0}/{1}' is not found".format(kLinks, kDstLinksDir)
    
    if not eLinks[kDstLinksDir]:
        return "Entry '{0}/{1}' is empty".format(kLinks, kDstLinksDir)

    # -------------------------------------------------------------------------
    if kTags in config:
        eTags = config[kTags]
        for k in eTags:
            if not k:
                return "Empty entry in '{0}'".format(kTags)
            if not eTags[k]:
                return "Entry value '{0}/{1}' is empty".format(kTags, k)

    # -------------------------------------------------------------------------
    if kDatabase not in config:
        return "Entry '{0}' is not found".format(kDatabase)

    eDatabase = config[kDatabase]
    
    if kDbFileName not in eDatabase:
        return "Entry '{0}/{1}' is not found".format(kDatabase, kDbFileName)

    if kDbBackupCount not in eDatabase:
        return "Entry '{0}/{1}' is not found".format(kDatabase, kDbBackupCount)
        if eDatabase[kDbBackupCount] < 0:
            return "Entry '{0}/{1}' <0".format(kDatabase, kDbBackupCount)

    if not eDatabase[kDbFileName]:
        return "Entry '{0}/{1}' is empty".format(kDatabase, kDbFileName)
    
#     if kLogging not in config:
#         return "Entry '{0}' is not found".format(kLogging)
    
    return None

def normalize(config):
    # -------------------------------------------------------------------------
    eFiles = config[kFiles]
    
    for eSrc in eFiles:
        eSrc[kSrcFileDir] = os.path.normpath(eSrc[kSrcFileDir])

        if (not kIncludeFiles in eSrc) or (not eSrc[kIncludeFiles]):
            eSrc[kIncludeFiles] = [u".*?"]

        if (not kIncludeDirs in eSrc) or (not eSrc[kIncludeDirs]):
            eSrc[kIncludeDirs] = [u".*?"]

    # -------------------------------------------------------------------------
    eLinks = config[kLinks]
    eLinks[kDstLinksDir] = os.path.normpath(eLinks[kDstLinksDir])

    # -------------------------------------------------------------------------
    if kTags in config:
        eTags = config[kTags]
        for k in eTags:
            eTags[k] = os.path.normpath(eTags[k])

    # -------------------------------------------------------------------------
    eDatabase = config[kDatabase]
    eDatabase[kDbFileName] = os.path.normpath(eDatabase[kDbFileName])
