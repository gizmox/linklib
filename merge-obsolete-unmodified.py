#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import time
import logging
import logging.config
import yaml

import cmdline
import config
import db
import utils

def main():
    startTime = time.time()

    utils.setupConsoleEncodig()

    utils.setupYamlLoader()
    args = cmdline.parseCommandLine()
    configFileName = os.path.abspath(args.d_configFileName)

    print("Config file: '{0}'".format(configFileName))
    
    cfg = {}
    error = config.loadFromFile(cfg, configFileName)
    if error is not None:
        print("Config file loading failed. " + error)
        return

    error = config.verify(cfg)
    if error is not None:
        print("Config verification failed. " + error)
        return

    config.normalize(cfg)

    # init logging
    if config.kLogging in cfg:
        logging.config.dictConfig(cfg[config.kLogging])

    logger = logging.getLogger(__name__)

    dbEntries = {}
    db.read(cfg[config.kDatabase][config.kDbFileName], dbEntries)

    resultDbEntries = {}
    if db.mergeObsoleteToUnmodified(resultDbEntries, dbEntries):
        db.write(cfg[config.kDatabase][config.kDbFileName],
                 resultDbEntries,
                 cfg[config.kDatabase][config.kDbBackupCount])

    logger.info(u"Finished in {0:.2f} sec.\n".
                format(time.time() - startTime))

if __name__ == "__main__":
    main();
