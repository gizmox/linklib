# -*- coding: utf-8 -*-

import argparse

def parseCommandLine():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", 
                        "--config",
                        required = True,
                        help = "config file name",
                        metavar = "ConfigFileName",
                        dest = "d_configFileName")
    return parser.parse_args()
    