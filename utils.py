# -*- coding: utf-8 -*-

import yaml
import codecs, sys

def construct_yaml_str(self, node):
    return self.construct_scalar(node)

def setupYamlLoader():
    yaml.Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
    yaml.SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)

def setupConsoleEncodig():
    outf = codecs.getwriter('utf-8')(sys.stdout, errors='replace')
    # sys.stdout = outf